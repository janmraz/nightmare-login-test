const Nightmare = require('nightmare')
const fs = require('fs')
const speakeasy = require('speakeasy')
const commandLineArgs = require('command-line-args')

console.time("concatenation");

main().then(() => console.log(true)).catch(e => {
  console.error(false)
  process.exit()
})

async function main() {

  const optionDefinitions = [
      { name: 'otp',alias: 'f', type: Boolean },
      { name: 'starturl', alias: 's', type: String },
      { name: 'room', alias: 'r', type: String },
      { name: 'username', alias: 'u', type: String },
      { name: 'password', alias: 'p', type: String },
      { name: 'otp_secret', alias: 'o', type: String }
  ]

  const options = commandLineArgs(optionDefinitions, { camelCase: true })

  const START_URL = options.starturl
  const ROOM =  options.room
  const USERNAME = options.username
  const PASSWORD = options.password
  const OTP_SECRET = options.otpSecret

  const nightmare = Nightmare({ waitTimeout: 3000 })

  await nightmare
    .goto(START_URL)
    .type('#user_instance', ROOM)
    .type('#user_login', USERNAME)
    .type('#user_password', PASSWORD)
    .click('[name=commit]')

  const TOKEN = await speakeasy.totp({
    secret: OTP_SECRET,
    encoding: 'base32'
  })

  await console.log('token',TOKEN,OTP_SECRET);

  if(options.otp){
    await nightmare
      .wait('.token')
      .type('.token', TOKEN)
      .click('[name=commit]')
      .wait(1000)
      .click('.logoff a')
      .wait(1000)
  }else{
    await nightmare
      .wait('.logoff a')
      .click('.logoff a')
      .wait(1000)
  }

  await nightmare.end()
}
