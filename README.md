# document-manager-status

End-to-End testing of [Document Manager](https://documentmanager.cz/)

## Installation

```
$ yarn install
```

## Normal Login

```
$ node index -s https://dev.documentmanager.cz -r dev -u jan.mraz -p tajneheslo123
```

## 2FA Login

```
$ node index -s https://dev.documentmanager.cz -r dev -u jan.mraz -p tajneheslo123 -o lyu57mvyhghkv5x6rxbnhr76 --otp
```

## Terminal options

| Option  |  Alias  | Type  | required |
| --------|---------|-------|-------|
| starturl | s  | String | Yes |
| room | r  | String | Yes |
| username | u  | String | Yes |
| password | p  | String | Yes |
| otp | f  | Bool | No |
| otp_secret | o  | String  | No |
